#!/bin/sh

programName=$(basename "$0")
STRANGLE_LIB_NAME="libstrangle.so"
STRANGLE_LIB_NAME_DLSYM="libstrangle_dlsym.so"

print_usage_info() {
  echo "Usage: ${programName} [OPTION]... [FPS] PROGRAM"
  echo "  or:  ${programName} [OPTION]... [FPS]:[BATTERY_FPS] PROGRAM"
  echo "Limit the framerate of PROGRAM to FPS"
  echo
  echo "Mandatory arguments to long options are mandatory for short options too."
  echo "  -f, --fps FPS            Limit the framerate to FPS."
  echo "                             A value of 0 will disable the limiter."
  echo
  echo "  -b, --battery-fps FPS    Alternative framerate cap for when running on"
  echo "                             battery power. See --fps"
  echo
  echo "  -a, --aniso AF           Sets the anisotropic filtering level."
  echo "                             Makes textures sharper at an angle."
  echo "                             Limited OpenGL support"
  echo
  echo "  -p, --picmip PICMIP      Sets the mipmap LoD bias to PICMIP."
  echo "                             A higher value means blurrier textures."
  echo "                             Can be negative"
  echo
  echo "  -k, --vulkan-only        Prevent the OpenGL libs from loading"
  echo
  echo "  -v, --vk-vsync VSYNC     Force vertical synchronisation on or off:"
  echo "                               0 - Force off"
  echo "                               1 - Mailbox mode. Uncapped framerate"
  echo "                               2 - Traditional vsync. Capped framerate"
  echo "                               3 - Adaptive vsync. tearing at low framerates"
  echo
  echo "  -t, --vk-trilinear       Force trilinear filtering."
  echo
  echo "  -g, --gl-vsync VSYNC     Force vertical synchronisation on or off:"
  echo "                               0 - Force off"
  echo "                               1 - Force on"
  echo "                               n - Sync to refresh rate / n"
  echo
  echo "  -d, --gl-dlsym           Enable dlsym hijacking in the OpenGL library"
  echo
  echo "Short options may not be combined into one string."
  echo "Options will override environment variables."
  echo
  echo "Some programs will crash, freeze or behave unexpectedly when dlsym is hijacked"
  echo "If a program is exhibiting these symptoms when run with strangle try disabling"
  echo "'--gl-dlsym' option. Or if the program uses the Vulkan API you can disable"
  echo "strangle's OpenGL libs altogether with the '--vulkan-only' option."
  echo
  echo "Strangle home page: <https://gitlab.com/torkel104/libstrangle>"
}

while [ $# -gt 0 ]; do
  if echo "$1" | grep -Pq '^\d+(\.\d+)?$'; then
    export STRANGLE_FPS="$1"
    shift
  elif echo "$1" | grep -Pq '^\d+(\.\d+)?:\d+(\.\d+)?$'; then
    STRANGLE_FPS="$(echo "$1" | cut -d':' -f1)"
    export STRANGLE_FPS

    STRANGLE_FPS_BATTERY="$(echo "$1" | cut -d':' -f2)"
    export STRANGLE_FPS_BATTERY
    shift
  else
    case "$1" in
    -f | --fps)
      export STRANGLE_FPS="$2"
      shift
      shift
      ;;
    -b | --battery-fps)
      export STRANGLE_FPS_BATTERY="$2"
      shift
      shift
      ;;
    -a | --aniso)
      export STRANGLE_AF="$2"
      shift
      shift
      ;;
    -p | --picmip)
      export STRANGLE_PICMIP="$2"
      shift
      shift
      ;;
    -k | --vulkan-only)
      STRANGLE_VK_ONLY="1"
      shift
      ;;
    -v | --vk-vsync)
      export STRANGLE_VK_VSYNC="$2"
      shift
      shift
      ;;
    -t | --vk-trilinear)
      export STRANGLE_VK_TRILINEAR="1"
      shift
      ;;
    -g | --gl-vsync)
      export STRANGLE_GL_VSYNC="${2}"
      shift
      shift
      ;;
    -d | --gl-dlsym)
      STRANGLE_GL_DLSYM="1"
      shift
      ;;
    -h | --help)
      print_usage_info
      exit 0
      ;;
    --)
      shift
      break 2
      ;;
    *)
      break 2
      ;;
    esac
  fi
done

if [ "$#" -eq 0 ]; then
  echo "${programName}: no program supplied"
  echo "Try '${programName} --help' for more information"
  exit 1
fi

if [ -n "${STRANGLE_AF}" ]; then
  # AMD
  export AMD_TEX_ANISO="${STRANGLE_AF}"
  export R600_TEX_ANISO="${STRANGLE_AF}"
  export RADV_TEX_ANISO="${STRANGLE_AF}"

  # Nvidia
  # http://us.download.nvidia.com/XFree86/Linux-x86_64/440.64/README/openglenvvariables.html
  __GL_LOG_MAX_ANISO="$(echo "${STRANGLE_AF}" | awk '{printf "%d", log($1)/log(2)}')"
  export __GL_LOG_MAX_ANISO
fi

if [ "$STRANGLE_VK_ONLY" != "1" ]; then
  if [ "$STRANGLE_GL_DLSYM" = "1" ]; then
    LD_PRELOAD="${LD_PRELOAD}:${STRANGLE_LIB_NAME_DLSYM}"
  else
    LD_PRELOAD="${LD_PRELOAD}:${STRANGLE_LIB_NAME}"
  fi
fi

# Execute the strangled program under a clean environment
# pass through the FPS and overriden LD_PRELOAD environment variables
exec env ENABLE_VK_LAYER_TORKEL104_libstrangle=1 LD_PRELOAD="${LD_PRELOAD}" "$@"
